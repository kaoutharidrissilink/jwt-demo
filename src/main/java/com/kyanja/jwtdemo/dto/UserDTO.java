package com.kyanja.jwtdemo.dto;

import java.io.Serializable;

public class UserDTO implements Serializable {


    private static final long serialVersionUID = 5863325673883042847L;


    private String username;
    private String password;
    private String role;


    public UserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
